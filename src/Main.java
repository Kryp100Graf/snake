import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class GameFrame extends JFrame {
    GameFrame() {
        this.add(new GamePanel());
        this.setTitle("Snake");
        this.setDefaultCloseOperation(3);
        this.setResizable(false);
        this.pack();
        this.setVisible(true);
        this.setLocationRelativeTo((Component) null);
    }
}

class GamePanel extends JPanel implements ActionListener {

    static final int SCREEN_WIDTH = 800;
    static final int SCREEN_HEIGHT = 600;
    static final int UNIT_SIZE = 25;
    static final int DELAY = 75;
    Timer timer;

    Point currentSnakePoint = new Point((SCREEN_WIDTH - 1) / 2 / UNIT_SIZE * UNIT_SIZE,
            (SCREEN_HEIGHT - 1) / 2 / UNIT_SIZE * UNIT_SIZE);
    public int lastKeyPressed = 0;

    public GamePanel() {
        this.setPreferredSize(new Dimension(SCREEN_WIDTH, SCREEN_HEIGHT));
        this.setBackground(Color.BLACK);
        this.setFocusable(true);
        this.addKeyListener(new SnakeKeyAdapter());

        timer = new Timer(DELAY, this);
        timer.start();
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.DARK_GRAY);

        drawGrid(g);
        drawSnake(g);

    }

    private void drawSnake(Graphics g) {
        g.setColor(Color.ORANGE);
        g.fillRect(currentSnakePoint.x, currentSnakePoint.y, UNIT_SIZE, UNIT_SIZE);
    }

    private void drawGrid(Graphics g) {
        for (int i = 0; i < SCREEN_HEIGHT; i += UNIT_SIZE) {
            g.drawLine(0, i, SCREEN_WIDTH, i);
        }
        for (int i = 0; i < SCREEN_WIDTH; i += UNIT_SIZE) {
            g.drawLine(i, 0, i, SCREEN_HEIGHT);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {



    }
}

class SnakeKeyAdapter extends KeyAdapter {
    @Override
    public void keyPressed(KeyEvent event) {
        System.out.println(event.getKeyCode());
    }
}

class Main {
    public static void main(String[] args) {
        new GameFrame();
    }
}
